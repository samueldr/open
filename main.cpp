/*
	open - Opens arguments to the configured utility.

	First draft uses xdg-open to open files.

	License:
		The MIT License (MIT)

		Copyright (c) 2014 Samuel Dionne-Riel

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.

	Authors:
		Samuel Dionne-Riel <samuel@dionne-riel.com>
*/
#include <cstdlib>
#include <unistd.h>

#include <iostream>
#include <queue>
#include <vector>

using std::string;

typedef struct options {
	bool edit_file;
	bool wait;
} options;

void print_help(string callee, bool short_help) {
	// TODO : Move help elsewhere; post-process it into code and man page.

	// Manpage for OSX's open utiliy.
	// https://developer.apple.com/library/mac/documentation/Darwin/Reference/Manpages/man1/open.1.html

	// Man synopsis (10.7 probably)
	//SYNOPSIS
	std::cout <<
	callee << " [-e] [-t] [-f] [-W] [-R] [-n] [-g] [-h] [--help]" << std::endl
	<< "    [-b bundle_identifier] [-a application] [file|URI] ..." << std::endl
	<< "    [--args arg1 ...]" << std::endl
	;
	if (short_help) {
		std::cout << std::endl << "Call with --help for the complete help." << std::endl;
		exit(0);
	}
	std::cout
	<< "" << std::endl
	<< "     -a application" << std::endl
	<< "         Specifies the application to use for opening the file" << std::endl
	<< "" << std::endl
	<< "     -b bundle_indentifier" << std::endl
	<< "         Unsupported on this platform; dropped." << std::endl
	<< "" << std::endl
	<< "     -e" << std::endl
	<< "         Causes the file to be opened with $EDITOR." << std::endl
	<< "" << std::endl
	<< "     -t" << std::endl
	<< "         Causes the file to be opened with the default text editor, as determined via $EDITOR." << std::endl
	<< "" << std::endl
	<< "     -f [ NOT IMPLEMENTED! ]" << std::endl
	<< "         Reads input from standard input and opens the results in the default text editor.  End input by" << std::endl
	<< "         sending EOF character (type Control-D).  Also useful for piping output to open and having it open" << std::endl
	<< "         in the default text editor." << std::endl
	<< "" << std::endl
	<< "     -W [ NOT IMPLEMENTED! ]" << std::endl
	<< "         Causes open to wait until the applications it opens (or that were already open) have exited.  Use" << std::endl
	<< "         with the -n flag to allow open to function as an appropriate app for the $EDITOR environment variable." << std::endl
	<< "" << std::endl
	<< "     -R [ NOT IMPLEMENTEDD! ]" << std::endl
	<< "         Reveals the file(s) in the Finder instead of opening them." << std::endl
	<< "" << std::endl
	<< "     -n" << std::endl
	<< "         Open a new instance of the application(s) even if one is already running." << std::endl
	<< "         On this platform, there is no control for instances, this parameter is silently dropped." << std::endl
	<< "" << std::endl
	<< "     -g [ NOT IMPLEMENTED! ]" << std::endl
	<< "         Do not bring the application to the foreground." << std::endl
	<< "         This might prove to be impossible on this platform." << std::endl
	<< "" << std::endl
	<< "     -h" << std::endl
	<< "         Searches header locations for a header whose name matches the given string and then opens it.  Pass" << std::endl
	<< "         a full header name (such as NSView.h) for increased performance." << std::endl
	<< "         Currently needs a proper complete header name; will not search... (such as stdlib.h or linux/vt.h)" << std::endl
	<< "" << std::endl
	<< "     --help" << std::endl
	<< "         Prints this help." << std::endl
	<< "" << std::endl
	<< "     --args" << std::endl
	<< "         All remaining arguments are passed to the opened application in the argv parameter to main()." << std::endl
	<< "         These arguments are not opened or interpreted by the open tool." << std::endl
	<< "" << std::endl
	;
	exit(0);
}
void print_help(string callee) {
	print_help(callee, false);
}

// C++-like exec.
int exec(std::string prog, std::vector<string> args) {
	// extra room for program name and sentinel
	const char **argv = new const char* [args.size()+2];

	argv[0] = prog.c_str();
	for (int i = 0; i < args.size()+1; ++i) {
		argv[i+1] = args[i] .c_str();
	}
	argv[args.size()+1] = NULL;

	return execvp(prog.c_str(), (char **)argv);
}

// Custom getenv()-like
bool env(const string key, string * val) {
	char* value = getenv(key.c_str());
	if (value) {
		*val = string(value);
		return true;
	}
	return false;
}

int main(int argc, char **argv) {
	// Default option values
	options opts = {
		.edit_file = false,
		.wait = false,
	};

	// I'd prefer to work C++ strings.
	std::queue<string> args;
	std::vector<string> args_to_open;
	for (int i=0; i<argc ;i++) {args.push(string(argv[i]));}
	// Program invocation
	string callee = args.front();args.pop();

	// TODO : Handle multiple short flags : -ge
	// TODO : Parse --long-arg=value ...
	while (!args.empty()) {
		string curr = args.front();
		args.pop();
		if (/* --reserved curr == "-h" || */
			curr == "--help" ||
			curr == "/?"
		){
			print_help(callee);
		}
		else if (curr == "-a" || curr == "--application") {
			curr = args.front();
			args.pop();
			std::cout << " -a is currently not supported '"
			<< curr <<"'" << std::endl;
			exit(1);
		}
		else if (curr == "-b") {
			//We silently drop -b and its parameter, irrelevant where there are no bundles.
			args.pop();
			continue;
		}
		else if (curr == "-e" ||
			curr == "-t" ||
			curr == "--edit" ||
			curr == "--text"
		) {
			opts.edit_file = true;
		}
		else if (curr == "-f") {
			std::cout << " -f is not yet implemented!" << std::endl
			<< " Work is needed to make it equivalent." << std::endl;
			exit(1);
		}
		else if (curr == "-W") {
			// -W will cause problem... we use $EDITOR in this scripts...
			// if $EDITOR is this script, it will loop. (detect!)
			std::cout << " -W not-implemented... dropped." << std::endl;
			opts.wait = true;
		}
		else if (curr == "-n") {
			// Opens a new instance of application(s) even if one is already running.
			// Dropped, irrelevant for now
			continue;
		}
		else if (curr == "-R") {
			// Need to find how we will reveal files.
			// THIS MAY ACTUALLY BE FILE-MANAGER DEPENDENT.
			// Mac OS X behaviour
			//  <-- When in the same folder, reveals only the last one
			//  <-- When two files in other folder... reveals the files, each in one window
			std::cout << " -R (reveal files) is currently unimplemented." << std::endl;
			exit(1);
		}
		else if (curr == "-g") {
			// Do not bring the application to the foreground.
			// This might prove to be impossible on this platform.
			// Is this even possible??
			// How could the application be controlled?
			std::cout << " -g option dropped." << std::endl;
			continue;
		}
		else if (curr == "-h") {
			// Easy way... but wrong
			// TODO : Fix -h to find .h in subfolders.
			curr = args.front();
			args.pop();
			args_to_open.push_back("/usr/include/" + curr);
		}
		// Defaults.
		else if (curr == "--") {
			// This means everything after -- is not parsed.
			while (!args.empty()) {
				curr = args.front();
				args.pop();
				args_to_open.push_back(curr);
			}
			break;
		}
		else if (curr[0] == '-') {
			std::cerr << "Unknown argument : " << curr << std::endl;
			exit(1);
		}
		// This allows out-of-order arguments passing.
		else {
			args_to_open.push_back(curr);
		}
	}

	// Check whether we had args or not.
	if (args_to_open.empty()) {
		std::cout << "No files or URI passed." << std::endl << std::endl;
		print_help(callee, true);
	}

	if (opts.edit_file) {
		string EDITOR = "";
		// Fallback on a known editor.
		if (!env("EDITOR", &EDITOR) || EDITOR.empty()) {
			EDITOR = "vim";
		}

		{ // Splits on spaces in EDITOR and adds to args.
			int currpos = EDITOR.size();
			int newpos = EDITOR.rfind(' ');
			while (newpos != string::npos) {
				string arg = EDITOR.substr(newpos+1, currpos);
				args_to_open.insert(args_to_open.begin(), arg);
				currpos = newpos-1;
				newpos = EDITOR.rfind(' ', currpos);
			}
			EDITOR = EDITOR.substr(0,currpos+1);
		}
		return exec(EDITOR, args_to_open);
	}

	exec("xdg-open", args_to_open);

    return 0;
}
